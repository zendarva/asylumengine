package xyz.theasylum.Tests;

import xyz.theasylum.engine.BaseGame;
import xyz.theasylum.engine.Scene;
import xyz.theasylum.engine.Window;
import xyz.theasylum.engine.node.CachedTexturedNode;
import xyz.theasylum.engine.node.LabelNode;
import xyz.theasylum.engine.node.Node;
import xyz.theasylum.engine.node.TexturedNode;
import xyz.theasylum.engine.resource.Texture;
import xyz.theasylum.engine.resource.TileSet;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

import static javax.swing.text.StyleConstants.Size;

public class TintTest {

    static Scene scene;

    public static void main(String args[]){
        Window window = new Window(new Dimension(960,540),"Testing", (dim)->{return new TestGame(dim);});
        window.start();

    }

    private static class TestGame extends BaseGame{
        LabelNode fpsLabel;
        Random random;
        private Dimension dim;

        public TestGame(Dimension dim) {
            super(dim);
            this.dim = dim;
            random = new Random(System.currentTimeMillis());
            scene = new Scene(dim);


            for (int x = 0; x < 800; x++) {
                scene.addChild(generateBottle());
            }


            fpsLabel = new LabelNode();
            fpsLabel.setSize(new Dimension(40,40));
            fpsLabel.setText("unknown");
            fpsLabel.setLocalPos(new Point(10,10));
            fpsLabel.setTopmost(true);
            scene.addChild(fpsLabel);
            pushScene(scene);
        }

        @Override
        protected void tick(long delta) {
            super.tick(delta);
            fpsLabel.setText(getFPS()+"");
        }

        private Bottle generateBottle(){
            Color color = new Color(random.nextInt(255),random.nextInt(255),random.nextInt(255));
            Bottle bottle = new Bottle(color);
            bottle.setLocalPos(new Point(random.nextInt(200),random.nextInt(200)));
            return bottle;
        }
    }




    private static class Bottle extends Node{
        TileSet set;
        Color myColor;
        CachedTexturedNode ctn;

        public Bottle(Color color){
            set = new TileSet("/Bottle.png", new Dimension(16,16));
            this.size=set.getTileSize();
            myColor=color;

            ctn = new CachedTexturedNode();
            ctn.setTexture(set);
        }

        @Override
        public void draw(Point offset, Graphics2D g) {

            Rectangle rect = getRect();
            rect.getLocation().translate(offset.x,offset.y);

            set.setTile(0);
            set.draw(g,rect);
            set.setTint(myColor);
            set.setTile(1);
            ctn.draw(rect.getLocation(),g);
            //super.draw(offset, g);
            set.setTint(null);
        }

    }

}
