package xyz.theasylum.engine;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xyz.theasylum.engine.input.KeyboardEvent;
import xyz.theasylum.engine.node.Node;
import xyz.theasylum.engine.node.action.Action;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferStrategy;
import java.util.*;

@Slf4j
public abstract class BaseGame extends Canvas implements Runnable {

    public static int displayScale = 2;

    protected boolean isRunning;


    private Deque<Scene> scenes;

    private Set<Action> actions;

    private Dimension size;

    private final int MAX_SAMPLES = 100;
    private int[] ticklist = new int[MAX_SAMPLES];
    private int tickPointer;
    private int tickSum;
    Image buffer;

    private int toPop = 0;


    protected Point mousePos;

    private Point2D.Float ratio = new Point2D.Float(2,2);

    public Point getMousePosGame(){
        return mousePos;
    }

    public Scene curScene(){
        return scenes.peekFirst();
    }

    public BaseGame(Dimension dim) {
        this.setMinimumSize(dim);
        this.setPreferredSize(dim);
        this.scenes = new ArrayDeque<Scene>();
        size = new Dimension(dim.width / displayScale, dim.height / displayScale);
        //size=dim;
        actions = new HashSet<>();
        setIgnoreRepaint(true);
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {

                ratio.x = (getWidth()/(float)size.width);
                ratio.y = getHeight()/(float)size.height;
            }
        });

        this.addMouseListener(new MouseHandler());
        this.addMouseMotionListener(new MouseHandler());
        this.addKeyListener(new KeyboardHandler());
    }

    public void runOnce(Action action, Node target) {
        actions.add(action);
        action.start(target);
    }

    private void doActions(long delta) {
        for (Action action : actions) {
            action.update(delta);
        }
        actions.removeIf(Action::isDone);

    }

    protected void tick(long delta) {
        calcFPS((int) delta);
    }


    public Scene createScene() {
        return new Scene(new Dimension(size.width, size.height));
    }

    public void pushScene(Scene scene) {
        scene.parentResized(size);
        scene.gameEngine=this;
        scenes.push(scene);
    }
    public void popScene(){
        toPop++;
    }

    private void calcFPS(int tick) {
        tickSum -= ticklist[tickPointer];
        tickSum += tick;
        ticklist[tickPointer] = tick;
        if (++tickPointer == MAX_SAMPLES)
            tickPointer = 0;
    }

    public float getFPS() {
        return (float) tickSum / MAX_SAMPLES;
    }

    @Override
    public void run() {
        long nextTick = 0;
        long lastTick = 0;
        long lastFrame = 0;
        createBufferStrategy(1);

        isRunning = true;

        buffer = this.getGraphicsConfiguration().createCompatibleVolatileImage(size.width, size.height, Transparency.TRANSLUCENT);
        //Image buffer = this.getGraphicsConfiguration().createCompatibleImage(size.width, size.height, Transparency.TRANSLUCENT);

//        RenderingHints rh
//                = new RenderingHints(RenderingHints.KEY_ANTIALIASING,
//                RenderingHints.VALUE_ANTIALIAS_ON);
//
//        rh.put(RenderingHints.KEY_RENDERING,
//                RenderingHints.VALUE_RENDER_QUALITY);

        while (isRunning) {
            if (toPop > 0) {
                for (int i = 0; i < toPop; i++) {
                    scenes.pop();
                }
                toPop=0;
            }
            long curTime = System.currentTimeMillis();
            long graphicsTick = curTime - lastFrame;
            lastFrame = System.currentTimeMillis();
            tick(graphicsTick);
            if (curTime > nextTick) {
                nextTick = curTime + 50;

                if (lastTick == 0) {
                    lastTick = curTime;

                } else {
                    for (Scene scene : scenes) {
                        scene.logicTick(curTime - lastTick);
                    }
                    lastTick = curTime;
                }
            }
            if (graphicsTick != curTime) {
                doActions(graphicsTick);
                for (Scene scene : scenes) {
                    scene.graphicsTick(graphicsTick);
                }
            }

            Graphics2D g = (Graphics2D) buffer.getGraphics();
            //g.setRenderingHints(rh);
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, size.width, size.height);
            Iterator<Scene> itr = scenes.descendingIterator();
            while(itr.hasNext()){
                itr.next().draw(new Point(0,0),g);
            }

            g.dispose();

            BufferStrategy strat = getBufferStrategy();
            if (strat == null) {
                createBufferStrategy(1);
                strat = getBufferStrategy();
            }
            g = (Graphics2D) getGraphics();
            g.drawImage(buffer, 0, 0, getWidth(), getHeight(), null);
            Thread.yield();
        }

        System.exit(0);
    }

    private class MouseHandler extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e) {

        }

        @Override
        public void mousePressed(MouseEvent e) {
            xyz.theasylum.engine.input.MouseEvent.MouseButtonEvent event = xyz.theasylum.engine.input.MouseEvent.MouseButtonEvent.builder()
                    .mouseLoc(new Point((int) (e.getPoint().x / ratio.x), (int) (e.getPoint().y / ratio.y)))
                    .pressed(true)
                    .button(e.getButton())
                    .build();
            mousePos=new Point(event.getMouseLoc());
            curScene().handleMouseClick(event);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            xyz.theasylum.engine.input.MouseEvent.MouseButtonEvent event = xyz.theasylum.engine.input.MouseEvent.MouseButtonEvent.builder()
                    .mouseLoc(new Point((int) (e.getPoint().x / ratio.x), (int) (e.getPoint().y / ratio.y)))
                    .pressed(false)
                    .button(e.getButton())
                    .build();
            mousePos=new Point(event.getMouseLoc());
            curScene().handleMouseClick(event);
        }



        @Override
        public void mouseMoved(MouseEvent e) {
            if (curScene() != null) {
                xyz.theasylum.engine.input.MouseEvent.MouseMoved mme = xyz.theasylum.engine.input.MouseEvent.MouseMoved.builder()
                                .loc(new Point((int) (e.getPoint().x / ratio.x), (int) (e.getPoint().y / ratio.y)))
                                        .build();
                mousePos=new Point(mme.getMouseLoc());
                curScene().handleMouseMove(mme);
            }
        }
    }
    private class KeyboardHandler extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            int mask = e.getModifiersEx();
            KeyboardEvent event = new KeyboardEvent(e.getKeyCode(),true);

            if ((KeyEvent.SHIFT_DOWN_MASK & mask) == InputEvent.SHIFT_DOWN_MASK) {
                event.setShiftDown(true);
            }
            curScene().unhandledInputEvent(event);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            KeyboardEvent event = new KeyboardEvent(e.getKeyCode(),false);
        }
    }
}
