package xyz.theasylum.engine.node;

import lombok.Getter;
import lombok.Setter;

import java.awt.*;

public class ProgressBar extends Node{

    @Getter
    @Setter
    protected int value;
    @Getter
    @Setter
    protected int max;
    @Getter
    @Setter
    protected Color completedColor = Color.GREEN;
    @Getter
    @Setter
    protected Color unfinishedColor = Color.RED;

    @Override
    public void draw(Point offset, Graphics2D g) {
        Rectangle rect = getRect();
        rect.translate(offset.x,offset.y);

        g.setColor(unfinishedColor);
        g.fillRect(rect.x,rect.y,rect.width, rect.height);
        g.setColor(completedColor);
        rect.width= (int) (size.width * (value/(float)max));
        g.fillRect(rect.x,rect.y,rect.width,rect.height);

        super.draw(offset,g);
    }
}
