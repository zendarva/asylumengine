package xyz.theasylum.engine.node;

import xyz.theasylum.engine.resource.TileSet;

import java.awt.*;
import java.util.HashMap;

public class TileMapNode extends Node {
    private TileSet set;

    HashMap<Point, Integer> mapData;

    public TileMapNode(TileSet set){
        this.set = set;
        this.selfManaged=false;
        mapData = new HashMap<>();
    }

    public void setTile(Point point, int tileNum){
        if (tileNum == -1)
            mapData.remove(point);
        else
            mapData.put(point,tileNum);
    }

    @Override
    public void draw(Point offset, Graphics2D g) {
        //Rectangle rect = getRect();

        Point minorOffset = new Point(offset.x % set.getTileSize().width, offset.y % set.getTileSize().height);

        Point tileOffset = new Point(offset.x/set.getTileSize().width,offset.y/set.getTileSize().height);

        for (int x = 0; x < (size.width/set.getTileSize().width)+2; x++) {
            for (int y = 0; y < (size.height/set.getTileSize().height)+2; y++) {
                Rectangle drawRect = new Rectangle(x*set.getTileSize().width+minorOffset.x,y*set.getTileSize().height+minorOffset.y,set.getTileSize().width,set.getTileSize().height);
                Point tilePoint = new Point(x-tileOffset.x,y-tileOffset.y);
                if (mapData.containsKey(tilePoint)) {
                    set.setTile(mapData.get(tilePoint));
                    set.draw(g, drawRect);
                }
            }
        }
//        super.draw(offset,g);
        children.forEach(f->f.draw(offset,g));
    }

    public Point mapToWorld(Point point){
        Point world = new Point((point.x*set.getTileSize().width),(point.y*set.getTileSize().height));
        return world;
    }

    public Point worldToMap(Point point){
        Point map = new Point(point.x/set.getTileSize().width, point.y/set.getTileSize().height);
        return map;
    }

    @Override
    public boolean contains(Point point) {
        return true;
    }
}
