package xyz.theasylum.engine.node;

import lombok.extern.slf4j.Slf4j;
import xyz.theasylum.engine.resource.Caching;
import xyz.theasylum.engine.resource.Texture;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Slf4j
public class CachedTexturedNode extends TexturedNode implements Caching {
    private BufferedImage buffer;
    private boolean dirty = true;
    private void createBuffer(){
        try {
            dirty=true;
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsConfiguration gc = ge.getDefaultScreenDevice().getDefaultConfiguration();
            buffer = gc.createCompatibleImage((int) size.getWidth(), (int) size.getHeight(), Transparency.TRANSLUCENT);
        }
        catch (Exception e){
            buffer = null;
            log.error("Shit");
        }
    }

    @Override
    public void setTexture(Texture texture) {
        super.setTexture(texture);
        createBuffer();
        dirty=true;
    }

    @Override
    public void draw(Point offset, Graphics2D g) {
        if (buffer== null)
            return;
        if (dirty){
            Graphics2D g2 = (Graphics2D) buffer.getGraphics();
            if (texture!=null) {
                Rectangle tempRect = new Rectangle(size);
                texture.draw(g2, tempRect);
            }
            children.stream().forEach(f->draw(new Point(0,0),g2));
            g2.dispose();
            dirty=false;
        }
        g.drawImage(buffer,offset.x+localPos.x,offset.y+localPos.y,null);

    }

    @Override
    public void setSize(Dimension size) {
        this.size=size;
        createBuffer();
    }

    @Override
    public void resize() {
        super.resize();
        createBuffer();
    }

    @Override
    public boolean isDirty() {
        return dirty;
    }

    @Override
    public void setDirty() {
        dirty=true;
        children.stream().filter(f->f instanceof Caching).map(f->(Caching)f).forEach(Caching::setDirty);
    }
}
