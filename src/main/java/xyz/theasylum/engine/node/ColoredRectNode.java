package xyz.theasylum.engine.node;

import lombok.Getter;
import lombok.Setter;

import java.awt.*;

public class ColoredRectNode extends Node {

    @Getter
    @Setter
    Color color;

    public void setSelfManaged(boolean managed){
        selfManaged=managed;
    }


    @Override
    public void draw(Point offset, Graphics2D g) {
        Rectangle rectangle = getRect();
        rectangle.translate(offset.x,offset.y);
        g.setColor(color);
        g.fillRect(rectangle.x,rectangle.y,rectangle.width, rectangle.height);
    }

    @Override
    public void parentResized(Dimension parentSize) {
        if (!selfManaged){
            localPos = new Point(0,0);
            size = new Dimension(parentSize);
        }
        super.parentResized(parentSize);
    }
}
