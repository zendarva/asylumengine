package xyz.theasylum.engine.node.action;

import xyz.theasylum.engine.node.Node;

public class ActionBounce extends Action{

    private final int xDelta;
    private final int yDelta;
    private final long interval;
    private long passed=0;
    private boolean add =false;
    private boolean done = false;

    public ActionBounce(final int xDelta, final int yDelta, final long interval){

        this.xDelta = xDelta;
        this.yDelta = yDelta;
        this.interval = interval;
    }

    @Override
    public void update(long delta) {
        passed+=delta;
        if (passed<interval)
                return;
        if(add)
            target.getLocalPos().translate(xDelta,yDelta);
        else
            target.getLocalPos().translate(-xDelta,-yDelta);
        add=!add;
        passed=0;
    }

    @Override
    public boolean isDone() {
        return done;
    }

    @Override
    public void abort() {
        if (!add)
            target.getLocalPos().translate(-xDelta,-yDelta); //Return to original position;
        done=true;
    }

    @Override
    public void start(Node node) {
        super.start(node);
        done=false;
    }
}
