package xyz.theasylum.engine.node.action;

import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import xyz.theasylum.engine.node.Node;

import java.util.Map;
import java.util.TreeMap;
import java.util.function.Consumer;

@Slf4j
public class ActionComplex extends Action {

    protected boolean repeat;

    private long runtime=0;
    private long longestRuntime;

    @Getter
    private boolean done=false;

    private TreeMap<Long, Consumer<Node>> actions;

    public void update(long delta){

        actions.forEach((key, value)->{
            if (key > runtime && key <= runtime+delta){
                value.accept(target);
            }
        });
        runtime+=delta;
        if (runtime>=longestRuntime){
            if (repeat==true)
                runtime=0;
            else
                done=true;
        }
    }

    @Builder
    private ActionComplex(@Singular Map<Long, Consumer<Node>> actions, boolean repeat){
        this.actions = new TreeMap<Long, Consumer<Node>>(actions);
        this.repeat = repeat;
        longestRuntime = this.actions.lastKey();
    }

    public void start(Node node){
        target = node;
        runtime=0;
        done=false;
    }
    public void abort(){
        if (!done) {
            runtime = longestRuntime;
            update(0);
        }
    }

}
