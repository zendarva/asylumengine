package xyz.theasylum.engine.node.action;

import xyz.theasylum.engine.node.Node;

public abstract class Action {
    protected Node target;

    public ActionCompleteCallback callback;

    public void start(Node node){
        target=node;
    }
    public abstract void update(long delta);

    public abstract boolean isDone();
    public void abort(){};
}
