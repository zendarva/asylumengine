package xyz.theasylum.engine.node.action;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import xyz.theasylum.engine.node.Node;

import java.awt.*;


@Slf4j
public class ActionMove extends Action{

    private Point destination;
    private int duration;
    private Point startPoint;
    private long passed=0;

    private boolean done=false;

    @Builder
    public ActionMove(Point destination, int duration, ActionCompleteCallback callback){
        this.destination = destination;
        this.duration = duration;
        this.callback=callback;
    }

    @Override
    public void update(long delta) {
        int oldX = lerp(startPoint.x, destination.x, (float)passed/duration);
        int oldY = lerp(startPoint.y, destination.y, (float)passed/duration);
        passed+=delta;
        int x = lerp(startPoint.x, destination.x, (float)passed/duration);
        int y = lerp(startPoint.y, destination.y, (float)passed/duration);

        target.getLocalPos().translate(x-oldX,y-oldY);

        if (passed > duration){
            done = true;
            target.setLocalPos(destination);
            if (null != callback){
                callback.done();
            }
        }

    }

    @Override
    public void abort() {
        target.setLocalPos(destination);
        if (null != callback){
            callback.done();
        }
        done=true;
    }

    private int lerp(int a, int b, float time){
        return (int) (a + time *(b-a));
    }

    @Override
    public void start(Node node) {
        super.start(node);
        startPoint=new Point(node.getLocalPos());
    }

    @Override
    public boolean isDone() {
        return done;
    }

}
