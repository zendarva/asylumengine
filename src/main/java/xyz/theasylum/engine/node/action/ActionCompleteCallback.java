package xyz.theasylum.engine.node.action;

public interface ActionCompleteCallback {
    public void done();
}
