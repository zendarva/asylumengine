package xyz.theasylum.engine.node.modifier;

import lombok.Builder;
import lombok.Singular;
import xyz.theasylum.engine.node.Node;
import xyz.theasylum.engine.node.action.Action;

import java.util.Map;

public class Timeline extends Modifier {

    private Node target;

    private Map<String, Action> actions;

    private Action current;

    @Override
    public void update(long delta) {
        if (current != null && !current.isDone()){
            current.update(delta);
        }
    }

    //Cleanup here...
    public void start(String name){
        if (actions.containsKey(name)){
            current= actions.get(name);
            current.start(target);
        }
    }

    @Builder
    private Timeline(Node target, @Singular Map<String, Action> actions) {
        this.target = target;
        this.actions = actions;
    }

    @Override
    public boolean isRunning() {
        return (null != current && current.isDone());
    }

    @Override
    public void abort() {
        if (isRunning())
            current.abort();
    }
}
