package xyz.theasylum.engine.node.modifier;

public abstract class Modifier {
    public void update (long delta){};
    public abstract boolean  isRunning();
    public abstract void abort();
}
