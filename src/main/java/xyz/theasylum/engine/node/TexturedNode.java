package xyz.theasylum.engine.node;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.css.Rect;
import xyz.theasylum.engine.resource.Texture;

import java.awt.*;

@Slf4j
public class TexturedNode extends Node{

    @Getter
    Texture texture;

    public void setTexture(Texture texture){
        this.texture=texture;
        this.size=texture.getRect().getSize();
    }
    public void setSelfManaged(boolean managed){
        this.selfManaged=managed;
    }

    @Override
    public void draw(Point offset, Graphics2D g) {
        if (!visible)
                return;
        if (texture!=null) {
            Rectangle tempRect = getRect();
            tempRect.translate(offset.x,offset.y);
            texture.draw(g, tempRect);
        }
        //This offset is wrong.  We need to look at it, it might work better to do it in node tho.
        super.draw(offset, g);
    }
}
