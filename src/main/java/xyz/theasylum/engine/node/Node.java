package xyz.theasylum.engine.node;

import lombok.Getter;
import lombok.Setter;
import xyz.theasylum.engine.input.InputEvent;
import xyz.theasylum.engine.input.KeyboardEvent;
import xyz.theasylum.engine.input.MouseEvent;
import xyz.theasylum.engine.input.interfaces.KeyboardHandler;
import xyz.theasylum.engine.input.interfaces.MouseButtonHandler;
import xyz.theasylum.engine.input.interfaces.MouseMoveHandler;
import xyz.theasylum.engine.node.action.Action;
import xyz.theasylum.engine.node.modifier.Modifier;
import xyz.theasylum.engine.utility.Anchor;

import java.awt.*;
import java.util.*;
import java.util.List;

public class Node {

    protected List<Node> children = new LinkedList<>();
    protected List<Modifier> modifiers= new LinkedList<>();

    @Getter
    @Setter
    EnumSet<Anchor> anchor = EnumSet.of(Anchor.Left, Anchor.Up);

    @Getter
    protected Node parent;

    @Getter
    protected boolean selfManaged =true;

    @Getter
    @Setter
    protected Point localPos = new Point(0,0);

    @Getter
    @Setter
    protected Dimension size= new Dimension(0,0);

    @Getter
    @Setter
    boolean fitWidth;

    @Getter
    @Setter
    protected boolean visible=true;

    @Getter
    @Setter
    protected boolean topmost;

    private Set<Action> actions = new HashSet<>();


    @Setter
    protected KeyboardHandler keyboardFallbackHandler = null;
    @Setter
    protected MouseButtonHandler mouseButtonFallbackHandler = null;

    @Setter
    protected MouseMoveHandler mouseMoveHandler;

    @Setter
    protected MouseButtonHandler mouseButtonHandler;

    public void setKeyboardFallbackHandler(KeyboardHandler handler){
        keyboardFallbackHandler = handler;
    }
    public void setMouseButtonFallbackHandler(MouseButtonHandler handler){
        mouseButtonFallbackHandler = handler;
    }

    public void draw(Point offset,Graphics2D g){
        Point point = new Point(offset);
        Point rectLoc = getRect().getLocation();
        point.translate(rectLoc.x, rectLoc.y);
        for (Node child : children) {
            child.draw(point,g);
        }
    }

    public void logicTick(long delta){}
    public void graphicsTick(long delta){
        children.forEach(f->f.graphicsTick(delta));
        modifiers.forEach(f->f.update(delta));
        actions.forEach(f-> f.update(delta));
        actions.removeIf(Action::isDone);
    }

    public void parentResized(Dimension parentSize){
        Dimension newSize;
        if (fitWidth){
            size.width=parentSize.width;
        }
        if (isSelfManaged())
            newSize= new Dimension(size);
        else
            newSize=new Dimension(parentSize);
        for (Node node : children){
            if (!node.isSelfManaged())
                node.setSize(newSize);
            node.parentResized(newSize);
        }
    }

    public Rectangle getRect(){
        Point anchorPoint = new Point(0,0);
        Point localPosMod = new Point(localPos);
        if (anchor.contains(Anchor.Down)){
            anchorPoint.translate(0, (int) parent.size.getHeight());
            anchorPoint.translate(0, (int) -size.getHeight());
            anchorPoint.translate(0,-(localPosMod.y*2));
        }


        if (anchor.contains(Anchor.Right)){
            anchorPoint.translate((int) parent.size.getWidth(),0);
            anchorPoint.translate((int) -size.getWidth(),0);
            anchorPoint.translate(-(localPosMod.x*2),0);
        }


        localPosMod.translate(anchorPoint.x,anchorPoint.y);
        if (anchor.contains(Anchor.Center_Horizontal)){
            localPosMod.x = parent.getSize().width/2-size.width/2;
        }
        if (anchor.contains(Anchor.Center_Vertical)){
            localPosMod.y = parent.getSize().height/2-size.height/2;
        }
        return new Rectangle(localPosMod,size);
    }

    public boolean handleMouseClick(MouseEvent.MouseButtonEvent event) {
        for (Node child : children) {
            if (child.contains(event.getMouseLoc())){
                Point point = new Point(event.getMouseLoc());
                Point rectLoc = child.getRect().getLocation();
                point.translate(-rectLoc.x,-rectLoc.y);
                MouseEvent.MouseButtonEvent mbe = MouseEvent.MouseButtonEvent.builder()
                        .button(event.getButton())
                        .pressed(event.isPressed())
                        .mouseLoc(point)
                        .build();
                if (child.handleMouseClick(mbe))
                    return true;
            }
        }
        if (null != mouseButtonHandler){
            return mouseButtonHandler.handleMouseButton(event);
        }
        return false;
    }

    public void handleMouseMove(MouseEvent.MouseMoved event){
        for (Node child : children) {
            if (child.contains(event.getMouseLoc())){
                Point point = new Point(event.getMouseLoc());
                point.translate(-child.localPos.x,-child.localPos.y);
                MouseEvent.MouseMoved mme = MouseEvent.MouseMoved.builder().loc(point).build();
                child.handleMouseMove(mme);
            }
        }
        if (null != mouseMoveHandler)
            mouseMoveHandler.handleMouseMove(event);
    }

    public void addChild(Node node){
        if (!node.isSelfManaged()){
            node.setSize(new Dimension(size));
            node.setLocalPos(new Point(0,0));
        }
        if (node.isTopmost())
            children.add(children.size(),node);
        else
            children.add(getInsertionLocation(),node);
        node.parent=this;
        node.parentResized(this.getSize());
    }
    private int getInsertionLocation(){
        for (int i = children.size() - 1; i >= 0; i--) {
            if (!children.get(i).isTopmost())
                return i+1;
        }
        return 0;
    }
    public List<Node> getChildren(){
        return Collections.unmodifiableList(children);
    }

    public void removeChild(Node node){
        if (children.contains(node))
            children.remove(node);
    }

    public void addModifier(Modifier modifier){
        modifiers.add(modifier);
    }

    public boolean unhandledInputEvent(InputEvent event){
        for (Node child : children) {
            if (child.unhandledInputEvent(event))
                return true;
        }
        if (event instanceof KeyboardEvent keyEvent && keyboardFallbackHandler!=null){
            return keyboardFallbackHandler.handleKeyboardEvent(keyEvent);
        }
        if (event instanceof MouseEvent.MouseButtonEvent mouseEvent && mouseButtonFallbackHandler!=null){
            return mouseButtonFallbackHandler.handleMouseButton(mouseEvent);
        }
        return false;

    }

    public void resize(){};


    public boolean contains(Point point){
        return this.getRect().contains(point);
    }


    public boolean stillAnimating(){
        for (Modifier modifier : modifiers) {
            if (modifier.isRunning()){
                return true;
            }
        }
        if (!actions.isEmpty())
                return true;
        for (Node child : children) {
            if (child.stillAnimating())
                return true;
        }

        return false;
    }

    public void stopAnimations(){
        for (Modifier modifier : modifiers) {
            modifier.abort();
        }
        actions.forEach(Action::abort);
        actions.clear();
        children.forEach(Node::stopAnimations);
    }

    public void runOnce(Action action){
        actions.add(action);
        action.start(this);
    }
}
