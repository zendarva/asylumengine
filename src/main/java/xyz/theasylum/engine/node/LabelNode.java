package xyz.theasylum.engine.node;

import lombok.Getter;
import lombok.Setter;
import xyz.theasylum.engine.resource.ResourceManager;

import java.awt.*;

public class LabelNode extends Node{

    @Getter
    @Setter
    String text;

    @Getter
    @Setter
    Color textColor = Color.red;

    @Getter
    @Setter
    Font font = ResourceManager.getFont("/glacial-indifference.regular.otf",12);

    @Override
    public void draw(Point offset, Graphics2D g) {
        Rectangle rect =getRect();
        rect.translate(offset.x,offset.y);
        if (null != text){
            g.setFont(font);
            g.setColor(textColor);
            int width = g.getFontMetrics().stringWidth(text);
            int height = g.getFontMetrics().getHeight();
            g.drawString(text,rect.x + size.width/2 - width/2,rect.y+rect.height/2+height/4);
        }

    }
}
