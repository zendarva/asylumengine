package xyz.theasylum.engine.node.container;

import lombok.extern.slf4j.Slf4j;
import xyz.theasylum.engine.input.MouseEvent;

import java.awt.*;
import java.awt.image.VolatileImage;


@Slf4j
public class ScrollableContainer extends Container{

    VolatileImage buffer;

    public Point scrollOffset = new Point(0,0);

    private void createBuffer(){
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsConfiguration gc = ge.getDefaultScreenDevice().getDefaultConfiguration();
            buffer = gc.createCompatibleVolatileImage((int) size.getWidth(), (int) size.getHeight(), Transparency.TRANSLUCENT);
        }
        catch (Exception e){
            buffer = null;
        }
    }

    @Override
    public void draw(Point offset, Graphics2D g) {
        if (buffer == null)
            return;
        Graphics2D g2 = (Graphics2D) buffer.getGraphics();
        g2.setColor(Color.BLACK);
        g2.fillRect(0,0,size.width,size.height);

        Rectangle rect = new Rectangle(scrollOffset,size);

        Point mirrorScrollOffset = new Point(-scrollOffset.x,-scrollOffset.y);

        children.stream().filter(f->rect.contains(f.getLocalPos()) || !f.isSelfManaged()).forEach(f->f.draw(mirrorScrollOffset,g2));
        g2.dispose();

        g.drawImage(buffer,offset.x+localPos.x,offset.y+localPos.y,null);
    }

    @Override
    public void parentResized(Dimension parentSize) {
        super.parentResized(parentSize);
        createBuffer();
    }

    @Override
    public boolean handleMouseClick(MouseEvent.MouseButtonEvent event) {
        MouseEvent.MouseButtonEvent childEvent = MouseEvent.MouseButtonEvent.builder()
                .button(event.getButton())
                .mouseLoc(event.getMouseLoc())
                .pressed(event.isPressed())
                .build();
                event.getMouseLoc().translate(scrollOffset.x,scrollOffset.y);
        return super.handleMouseClick(childEvent);
    }
    @Override
    public void handleMouseMove(MouseEvent.MouseMoved event) {
        MouseEvent.MouseMoved mme = MouseEvent.MouseMoved.builder().loc(event.getMouseLoc()).build();
        mme.getMouseLoc().translate(scrollOffset.x, scrollOffset.y);
        super.handleMouseMove(event);
    }


    public void center(Point point){
        Point newCenter = new Point(point);
        newCenter.translate(-size.width/2,-size.height/2);
        scrollOffset=newCenter;
    }

    @Override
    public void resize() {
        super.resize();
        createBuffer();
    }

}
