package xyz.theasylum.engine.node.container;

import xyz.theasylum.engine.input.InputEvent;
import xyz.theasylum.engine.input.KeyboardEvent;
import xyz.theasylum.engine.node.Node;

import java.awt.*;
import java.awt.event.KeyEvent;

public class HorizontalSplitContainer extends Container{

    private void updateChildPositions(){
        int unmodifiyableHeight=0;
        int modifyableChildren=0;

        Rectangle rect = getRect();

        for(Node child: children){
            if (child.isSelfManaged())
                unmodifiyableHeight += child.getSize().height;
            else
                modifyableChildren++;
        }

        int availableHeight = size.height-unmodifiyableHeight;
        int childHeight = 0;

        if (modifyableChildren > 0)
            childHeight= availableHeight/modifyableChildren;

        int y = rect.y;
        for (Node child : children) {
            child.setLocalPos(new Point(0, y));
            if (child.isSelfManaged()) {
                y += child.getSize().height;
                if (child.isFitWidth()){
                    child.getSize().width=(int) rect.getWidth();
                }
            }
            else {
                y += childHeight;
                child.getSize().height = childHeight;
                child.getSize().width = getSize().width;
                child.resize();
            }
        }
    }

    @Override
    public void addChild(Node node) {
        super.addChild(node);
        updateChildPositions();
    }
    @Override
    public void parentResized(Dimension parentSize) {
        super.parentResized(parentSize);
        updateChildPositions();
    }

    @Override
    public void resize() {
        updateChildPositions();
    }

    @Override
    public void draw(Point offset, Graphics2D g) {
        super.draw(offset, g);
    }

}
