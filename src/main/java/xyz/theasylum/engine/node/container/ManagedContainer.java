package xyz.theasylum.engine.node.container;

import xyz.theasylum.engine.Scene;
import xyz.theasylum.engine.input.MouseEvent;
import xyz.theasylum.engine.node.Node;

import java.awt.*;

public class ManagedContainer extends Container {
    public ManagedContainer() {
        this.selfManaged=true;
    }

    @Override
    public void draw(Point offset, Graphics2D g) {
        super.draw(offset,g);
    }


}
