package xyz.theasylum.engine.node.container;

import xyz.theasylum.engine.node.Node;

import java.awt.*;

public class VerticalSplitContainer extends Container {


    @Override
    public void addChild(Node node) {
        super.addChild(node);
        updateChildPositions();
    }

    private void updateChildPositions() {
        int unmodifyableWidth = 0;
        int modifyableChildren = 0;

        Rectangle rect = getRect();

        for (Node child : children) {
            if (child.isSelfManaged()) {
                unmodifyableWidth += child.getSize().width;
            } else
                modifyableChildren++;
        }

        int remainingWidth = size.width - unmodifyableWidth;
        int childWidth = 0;
        if (modifyableChildren > 0)
            childWidth = remainingWidth / modifyableChildren;

        int x = rect.x;
        for (Node child : children) {
            child.setLocalPos(new Point(x, 0));
            if (child.isSelfManaged()) {
                x += child.getSize().width;
            } else {
                x += childWidth;
                child.getSize().width = childWidth;
                child.getSize().height = getSize().height;
                child.resize();
            }

        }
    }

    @Override
    public void parentResized(Dimension parentSize) {
        super.parentResized(parentSize);
        updateChildPositions();
    }

    @Override
    public void resize() {
        updateChildPositions();
    }
}
