package xyz.theasylum.engine.node;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import xyz.theasylum.engine.input.MouseEvent;
import xyz.theasylum.engine.resource.ResourceManager;
import xyz.theasylum.engine.resource.Texture;

import java.awt.*;
import java.util.function.Consumer;
import java.util.function.Supplier;

@Slf4j
public class ButtonNode extends Node {

    @Getter
    @Setter
    Texture pressed;

    @Getter
    Texture def;

    @Getter
    @Setter
    String label;


    @Setter
    Supplier<Boolean> onClick;

    public void setDef(Texture texture){
        this.def=texture;
        curTexture=def;
    }

    Texture curTexture;

    @Override
    public boolean handleMouseClick(MouseEvent.MouseButtonEvent event) {
        if (super.handleMouseClick(event))
            return true;
        if (event.getButton() !=1)
                return false;
        if (event.isPressed()){
            if (curTexture == def && null != pressed)
                curTexture=pressed;
        }
        else
        {
            curTexture=def;
            if (null != onClick)
                return onClick.get();
        }
        return true;
    }

    @Override
    public void draw(Point offset, Graphics2D g) {
        if (curTexture !=null){
            Rectangle rect = getRect();
            rect.translate(offset.x,offset.y);
            curTexture.draw(g, rect);
            if (null != label){
                Font font = ResourceManager.getFont("/glacial-indifference.regular.otf",12);
                g.setFont(font);
                g.setColor(Color.RED);
                int width = g.getFontMetrics().stringWidth(label);
                int height = g.getFontMetrics().getHeight();
                g.drawString(label,rect.x + size.width/2 - width/2,rect.y+rect.height/2+height/4);
            }

        }
    }
}
