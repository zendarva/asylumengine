package xyz.theasylum.engine.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class ResourceManager {

    private static Logger logger = LoggerFactory.getLogger(ResourceManager.class);

    private static Map<String, BufferedImage> images = new HashMap<>();
    private static Map<String, Font> fonts = new HashMap<>();

    public static BufferedImage loadImage(String path){

        if (images.containsKey(path))
                return images.get(path);

        BufferedImage image;
        try {
            InputStream is = ResourceManager.class.getResourceAsStream(path);
            if (is== null)
                throw new FileNotFoundException();
            image =  ImageIO.read(is);
            return image;
        } catch (IOException e) {
            logger.debug("Attempted to load missing resource {} from jar", path);
        }

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(path);
            image = ImageIO.read(fis);
        } catch (FileNotFoundException e) {
            logger.debug("Attempted to load missing resource {} from filesystem", path);
        } catch (IOException e) {
            logger.debug("Error loading resource {} from filesystem: {}", path, e);
        }
        //Eventually, we will return a default texture here.
        return null;
    }

    public static Font getFont(String name, int size){

        if (fonts.containsKey(name+":"+size))
            return fonts.get(name+":"+size);
        try {
            Font font = Font.createFont(Font.TRUETYPE_FONT, ResourceManager.class.getResourceAsStream(name)).deriveFont((float)size);
            fonts.put(name+":"+size,font);
            return font;
        } catch (FontFormatException e) {
        } catch (IOException e) {
        }
        Font font = new Font(name,Font.TRUETYPE_FONT,size);
        fonts.put(name+":"+size,font);
        return font;
    }
}
