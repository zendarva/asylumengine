package xyz.theasylum.engine.resource;

import lombok.Getter;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class TileSet extends Texture {

    private List<Rectangle> tiles;
    @Getter
    private Dimension tileSize;
    @Getter
    private int tile;

    public TileSet(String path, Dimension tileSize) {
        super(path);
        this.tileSize = tileSize;
        tiles = new ArrayList<>();
        int i=0;
        for (int y = 0; y < texture.getHeight()/tileSize.height; y++) {
            for (int x = 0; x < texture.getWidth()/ tileSize.width; x++) {
              tiles.add(i, new Rectangle(x*tileSize.width,y*tileSize.height,tileSize.width,tileSize.height));
              i++;
            }
        }
    }
    public void setTile(int tileNum){
        if (tileNum > tiles.size()-1)
            tile =0;
        else
            tile =tileNum;
    }

    @Override
    public Rectangle getRect() {
        return tiles.get(tile);
    }
}
