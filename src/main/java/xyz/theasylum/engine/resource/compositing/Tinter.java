package xyz.theasylum.engine.resource.compositing;

import lombok.extern.slf4j.Slf4j;

import java.awt.*;
import java.awt.image.ColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;

@Slf4j
public class Tinter implements Composite {
    private Color color;

    public Tinter(Color color) {
        this.color = color;
    }

    @Override
    public CompositeContext createContext(ColorModel srcColorModel, ColorModel dstColorModel, RenderingHints hints) {
        return new TintContext(color);
    }

    public static class TintContext implements CompositeContext {

        private Color color;
        private float[] colorFloat;

        public TintContext(Color color) {
            this.color = color;
            colorFloat = color.getColorComponents(new float[3]);
            for (int i = 0; i < 3; i++) {
                colorFloat[i] = colorFloat[i] * 255;
            }
        }

        @Override
        public void dispose() {

        }

        public void compose(Raster src, Raster dstIn, WritableRaster dstOut) {
            int w = Math.min(src.getWidth(), dstIn.getWidth());
            int h = Math.min(src.getHeight(), dstIn.getHeight());

            int[] data = new int[(w * h) * 4];

            float[] outRGBA = new float[4];
            outRGBA[3] = 255;
            data = src.getPixels(0, 0, w, h, data);

            for (int x = 0; x < w; x++) {
                for (int y = 0; y < h; y++) {
                    int index = (x + (y * w))*4;
                    if (data[index + 3] == 0)
                        continue;
                    float num = (255 - data[index]) / 255f;
                    for (int i = 0; i < 3; i++) {
                        outRGBA[i] = (int) (num * colorFloat[i]);
                    }
                    dstOut.setPixel(x,y,outRGBA);
                }
            }

        }

    }
}
