package xyz.theasylum.engine.resource;

import lombok.Getter;
import lombok.Setter;
import xyz.theasylum.engine.resource.compositing.Tinter;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

public class Texture {

    final public BufferedImage texture;
    final Rectangle rect;

    @Getter
    @Setter
    protected Color tint;

    private AffineTransform transform = AffineTransform.getScaleInstance(1,1);


    public Texture(final String path){
        texture = ResourceManager.loadImage(path);
        rect = new Rectangle(0,0,texture.getWidth(),texture.getHeight());
    }

    public Rectangle getRect(){
        return rect;
    }

    public void flipX(){
        transform.scale(-1,transform.getScaleY());
    }
    public void flipY(){
        transform.scale(transform.getScaleX(),-1);
    }


    public void draw(Graphics2D g, Rectangle targ){
        Rectangle lRect = getRect();
        Point translate = calcTranslation(targ);

        AffineTransform oldTransform = new AffineTransform(g.getTransform());
        g.translate(translate.x, translate.y);
        g.transform(transform);


        if (tint!=null){
            Composite def = g.getComposite();
            g.setComposite(new Tinter(tint));
            g.drawImage(texture,0,0,targ.width,targ.height,lRect.x,lRect.y,lRect.x+lRect.width,lRect.y+lRect.height,null);
            g.setComposite(def);

//            g.drawImage(texture,0,0,targ.width,targ.height,lRect.x,lRect.y,lRect.x+lRect.width,lRect.y+lRect.height,null);
//            g.setXORMode(new Color(tint.getRed(),tint.getGreen(),tint.getBlue(),0));
//            g.drawImage(texture,0,0,targ.width,targ.height,lRect.x,lRect.y,lRect.x+lRect.width,lRect.y+lRect.height,null);
//            g.setXORMode(Color.black);
        }
        else
            g.drawImage(texture,0,0,targ.width,targ.height,lRect.x,lRect.y,lRect.x+lRect.width,lRect.y+lRect.height,null);

        g.setTransform(oldTransform);
    }

    private Point calcTranslation(Rectangle targ){
        Point translate = new Point(targ.getLocation());
        if (transform.getScaleX() < 0)
            translate.x+=targ.width;
        if (transform.getScaleY() <0)
            translate.y+=targ.height;
        return translate;
    }

}
