package xyz.theasylum.engine.resource;

import java.awt.*;

public class NinePatch extends Texture {

    private final int tileWidth;
    private final int tileHeight;
    Point[] patches;

    public NinePatch(String path) {
        super(path);
        patches = new Point[9];
        tileWidth = texture.getWidth() / 3;
        tileHeight = texture.getHeight() / 3;
        int i = 0;
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                patches[i] = new Point(x * tileWidth, y * tileHeight);
                i++;
            }
        }
    }

    @Override
    public void draw(Graphics2D g, Rectangle targ) {

        for (int x = tileWidth; x < targ.width-tileWidth; x+=tileWidth) {
            for (int y = tileHeight; y < targ.height-tileHeight; y+=tileHeight) {
                drawTile(g,new Point(targ.x+x,targ.y+y),4);
            }
        }

        for (int x = tileWidth; x < targ.width-tileWidth; x+=tileWidth) {
            drawTile(g,new Point(targ.x+x,targ.y),1);
            drawTile(g,new Point(targ.x+x, targ.y+targ.height-tileHeight),7);
        }

        for (int y = tileHeight; y < targ.height-tileHeight; y+=tileHeight) {
            drawTile(g,new Point(targ.x,targ.y+y),3);
            drawTile(g,new Point(targ.x+targ.width-tileWidth,targ.y+y),5);
        }

        //Corners
        drawTile(g,new Point(targ.x,targ.y),0);
        drawTile(g,new Point(targ.x+targ.width-tileWidth,targ.y),2);
        drawTile(g,new Point(targ.x,targ.y+targ.height-tileHeight),6);
        drawTile(g,new Point(targ.x+targ.width-tileWidth,targ.y+targ.height-tileHeight),8);

    }
    private void drawTile(Graphics g, Point at, int tileNum){
        g.drawImage(texture, at.x, at.y, at.x + tileWidth, at.y + tileHeight, patches[tileNum].x, patches[tileNum].y, patches[tileNum].x+tileWidth, patches[tileNum].y+tileHeight, null);
    }
}
