package xyz.theasylum.engine.resource;

public interface Caching {
    public boolean isDirty();
    public void setDirty();
}
