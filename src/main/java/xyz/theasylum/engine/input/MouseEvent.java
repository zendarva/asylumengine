package xyz.theasylum.engine.input;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.awt.*;

public class MouseEvent extends InputEvent {
    @Getter
    @Setter
    protected Point mouseLoc;

    @Data
    public static class MouseButtonEvent extends MouseEvent{
        protected int button;
        protected boolean pressed;

        @Builder
        public MouseButtonEvent(int button, boolean pressed, Point mouseLoc) {
            this.button = button;
            this.pressed = pressed;
            this.mouseLoc=mouseLoc;
        }
    }


    public static class MouseMoved extends MouseEvent{
        @Builder
        public MouseMoved(Point loc) {
            mouseLoc=loc;
        }
    }
}
