package xyz.theasylum.engine.input;

import lombok.Data;

@Data
public class KeyboardEvent extends InputEvent {
    int keycode;
    boolean down;

    boolean shiftDown;

    public KeyboardEvent(int keycode, boolean down) {
        this.keycode = keycode;
        this.down = down;
    }
}
