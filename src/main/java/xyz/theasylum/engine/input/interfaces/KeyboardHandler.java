package xyz.theasylum.engine.input.interfaces;

import xyz.theasylum.engine.input.KeyboardEvent;

public interface KeyboardHandler {
    boolean handleKeyboardEvent(KeyboardEvent event);
}
