package xyz.theasylum.engine.input.interfaces;

import xyz.theasylum.engine.input.MouseEvent;

public interface MouseButtonHandler {
    boolean handleMouseButton(MouseEvent.MouseButtonEvent event);
}
