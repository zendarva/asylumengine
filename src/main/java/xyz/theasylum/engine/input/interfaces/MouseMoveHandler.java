package xyz.theasylum.engine.input.interfaces;

import xyz.theasylum.engine.input.MouseEvent;

public interface MouseMoveHandler {
    public void handleMouseMove(MouseEvent.MouseMoved event);
}
