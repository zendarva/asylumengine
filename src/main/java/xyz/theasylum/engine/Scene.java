package xyz.theasylum.engine;

import lombok.Getter;
import xyz.theasylum.engine.input.InputEvent;
import xyz.theasylum.engine.input.KeyboardEvent;
import xyz.theasylum.engine.input.MouseEvent;
import xyz.theasylum.engine.node.Node;
import xyz.theasylum.engine.node.action.Action;
import xyz.theasylum.engine.node.modifier.Modifier;

import java.awt.*;
import java.util.*;
import java.util.List;

public class Scene extends Node {

    public BaseGame gameEngine;

    private Queue<InputEvent> inputEvents = new LinkedList<>();

    public Scene(Dimension dim) {
        this.size = dim;
    }

    @Override
    public void graphicsTick(long delta) {
        try {
            if (!inputEvents.isEmpty()) {
                List<InputEvent> events = new ArrayList<>(inputEvents);
                inputEvents.clear();
                for (InputEvent inputEvent : events) {
                    if (inputEvent instanceof MouseEvent.MouseButtonEvent) {
                        super.handleMouseClick((MouseEvent.MouseButtonEvent) inputEvent);
                    } else if (inputEvent instanceof MouseEvent.MouseMoved) {
                        super.handleMouseMove((MouseEvent.MouseMoved) inputEvent);
                    } else if (inputEvent instanceof KeyboardEvent) {
                        super.unhandledInputEvent(inputEvent);
                    }
                }
            }
        }
        catch(ArrayIndexOutOfBoundsException JavaIsStupid){

        }
        super.graphicsTick(delta);
    }

    @Override
    public void parentResized(Dimension parentSize) {
        size = new Dimension(parentSize);
        super.parentResized(parentSize);
    }


    @Override
    public boolean handleMouseClick(MouseEvent.MouseButtonEvent event) {
        inputEvents.add(event);
        return true;
    }

    @Override
    public void handleMouseMove(MouseEvent.MouseMoved event) {
        inputEvents.add(event);
    }

    @Override
    public boolean unhandledInputEvent(InputEvent event) {
        inputEvents.add(event);
        return true;
    }
}
