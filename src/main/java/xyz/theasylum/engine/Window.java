package xyz.theasylum.engine;

import javax.swing.*;
import java.awt.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Stream;

public class Window {

    private final Dimension dimension;
    private final String title;
    private BaseGame game;

    public Window(Dimension dimension, String title, Function<Dimension, BaseGame> producer){

        System.setProperty("sun.java2d.opengl","true");
        System.setProperty("sun.java2d.uiScale", "1");

        this.dimension = dimension;
        this.title = title;
        JFrame frame = new JFrame(title);
        this.game = producer.apply(dimension);
        frame.setIgnoreRepaint(true);
        frame.setSize(dimension);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(game);
        frame.setVisible(true);
    }

    public void start(){
        Thread thread = new Thread(game);
        thread.start();
    }

    private static long square(Rectangle rec) {
        return Math.abs(rec.width * rec.height);
    }
}
