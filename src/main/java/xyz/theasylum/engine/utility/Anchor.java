package xyz.theasylum.engine.utility;

public enum Anchor {
    Up,
    Down,
    Left,
    Right,
    Center_Horizontal,
    Center_Vertical,
}
